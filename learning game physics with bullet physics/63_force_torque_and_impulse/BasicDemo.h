#pragma once
#include "BulletOpenGLApplication.h"

#define EXPLOSION_STRENGTH 50.0f

class BasicDemo : public BulletOpenGLApplication
{
public:
	BasicDemo();
	void InitializePhysics() override;
	void ShutdownPhysics() override;
	void CreateObjects();
	void Keyboard(unsigned char key, int x, int y) override;
	void KeyboardUp(unsigned char key, int x, int y) override;
	void UpdateScene(float dt) override;
	void CollisionEvent(btRigidBody * body0, btRigidBody * body1) override;

protected:
	GameObject * _box;
	btCollisionObject * _trigger;
	bool _applyForce;
	btCollisionObject * _explosion;
	bool _canExplode;
};
#pragma once
#include <bullet/LinearMath/btIDebugDraw.h>

class DebugDrawer : public btIDebugDraw
{
public:
	void setDebugMode(int debugMode) override {_debugMode = debugMode;}
	int getDebugMode() const override {return _debugMode;}

	// drawing functions
	void drawContactPoint(btVector3 const & pointOnB, btVector3 const & normalOnB, 
		btScalar distance, int lifeTime, btVector3 const & color) override;

	void drawLine(btVector3 const & from, btVector3 const & to, btVector3 const & color) override;

	// unused
	void reportErrorWarning(char const * warningString) override {}
	void draw3dText(btVector3 const & location, char const * textString) override {}

	void ToggleDebugFlag(int flag);

protected:
	int _debugMode;
};

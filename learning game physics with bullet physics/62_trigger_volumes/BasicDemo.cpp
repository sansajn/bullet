#include "BasicDemo.h"
#include <bullet/btBulletDynamicsCommon.h>


void BasicDemo::InitializePhysics()
{
	_collisionConfiguration = new btDefaultCollisionConfiguration{};
	_dispatcher = new btCollisionDispatcher{_collisionConfiguration};
	_broadphase = new btDbvtBroadphase{};
	_solver = new btSequentialImpulseConstraintSolver{};
	_world = new btDiscreteDynamicsWorld{_dispatcher, _broadphase, _solver, _collisionConfiguration};
	CreateObjects();
}

void BasicDemo::ShutdownPhysics()
{
	delete _world;
	delete _solver;
	delete _broadphase;
	delete _dispatcher;
	delete _collisionConfiguration;
}

void BasicDemo::CreateObjects()
{
	// plane
	CreateGameObject(new btBoxShape{btVector3{1,50,50}}, 0, btVector3{0.2f, 0.6f, 0.6f}, btVector3{0.0f, 0.0f, 0.0f});

	// cube1
	_box = CreateGameObject(new btBoxShape(btVector3{1,1,1}), 1, btVector3{1, 0.2f, 0.2f}, btVector3{0, 10 ,0});

	// cube2
	CreateGameObject(new btBoxShape{btVector3{1,1,1}}, 1.0, btVector3{0.0f, 0.2f, 0.8f}, btVector3{1.25f, 20.0f, 0.0f});

	_trigger = new btCollisionObject();
	_trigger->setCollisionShape(new btBoxShape{btVector3{1, 0.2f, 1}});
	btTransform triggerTrans;
	triggerTrans.setIdentity();
	triggerTrans.setOrigin(btVector3{0, 1.5f, 0});
	_trigger->setWorldTransform(triggerTrans);
	_trigger->setCollisionFlags(btCollisionObject::CF_NO_CONTACT_RESPONSE);
	_world->addCollisionObject(_trigger);
}

void BasicDemo::CollisionEvent(btRigidBody * body0, btRigidBody * body1)
{
	if (body0 == _box->GetRigidBody() && body1 == _trigger || body1 == _box->GetRigidBody() && body0 == _trigger)
		CreateGameObject(new btBoxShape{btVector3{2,2,2}}, 2, btVector3{0.3, 0.7, 0.3}, btVector3{5, 10, 0});
}

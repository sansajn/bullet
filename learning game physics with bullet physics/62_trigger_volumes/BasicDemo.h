#pragma once
#include "BulletOpenGLApplication.h"

class BasicDemo : public BulletOpenGLApplication
{
public:
	void InitializePhysics() override;
	void ShutdownPhysics() override;
	void CreateObjects();
	void CollisionEvent(btRigidBody * body0, btRigidBody * body1) override;

protected:
	GameObject * _box;
	btCollisionObject * _trigger;
};
#pragma once
#include <vector>
#include <set>
#include <iterator>
#include <algorithm>
#include <GL/freeglut.h>
#include <bullet/BulletDynamics/Dynamics/btDynamicsWorld.h>
#include "OpenGLMotionState.h"
#include "GameObject.h"
#include "DebugDrawer.h"

using GameObjects = std::vector<GameObject *>;
using CollisionPair = std::pair<btRigidBody const *, btRigidBody const *>;
using CollisionPairs = std::set<CollisionPair>;

struct RayResult
{
	btRigidBody * body;
	btVector3 hitPoint;
};

class BulletOpenGLApplication
{
public:
	BulletOpenGLApplication();
	virtual ~BulletOpenGLApplication();
	
	// window events
	virtual void Initialize();
	virtual void Keyboard(unsigned char key, int x, int y);
	virtual void KeyboardUp(unsigned char key, int x, int y);
	virtual void Special(int key, int x, int y);
	virtual void SpecialUp(int key, int x, int y);
	virtual void Reshape(int w, int h);
	virtual void Idle();
	virtual void Mouse(int button, int state, int x, int y);
	virtual void PassiveMotion(int x, int y);
	virtual void Motion(int x, int y);
	virtual void Display();

	virtual void RenderScene();
	virtual void UpdateScene(float dt);

	// physics functions
	virtual void InitializePhysics();
	virtual void ShutdownPhysics();
	
	// camera functions
	void UpdateCamera();
	void RotateCamera(float & angle, float value);
	void ZoomCamera(float distance);

	// drawing functions
	void DrawBox(btVector3 const & halfSize);
	void DrawShape(btScalar * transform, btCollisionShape const * shape, btVector3 const & color);

	GameObject * CreateGameObject(btCollisionShape * shape, float mass,
		btVector3 const & color = btVector3{1,1,1},
		btVector3 const & initialPosition = btVector3{0,0,0},
		btQuaternion const & initialRotation = btQuaternion{0,0,1,1});

	void ShootBox(btVector3 const & derection);
	void DestroyGameObject(btRigidBody * body);
	GameObject * FindGameObject(btRigidBody * body);

	// picking functions
	btVector3 GetPickingRay(int x, int y);
	bool Raycast(btVector3 const & startPosition, btVector3 const & direction, RayResult & output);

	// constraints functions
	void CreatePickingConstraint(int x, int y);
	void RemovePickingConstraint();

	// collisionevent functions
	void CheckForCollisionEvents();
	virtual void CollisionEvent(btRigidBody * body0, btRigidBody * body1);
	virtual void SeparationEvent(btRigidBody * body0, btRigidBody * body1);

protected:
	btVector3 _cameraPosition;
	btVector3 _cameraTarget;
	float _nearPlane;
	float _farPlane;
	btVector3 _upVector;
	float _cameraDistance;
	float _cameraPitch;
	float _cameraYaw;

	int _screenWidth;
	int _screenHeight;

	btBroadphaseInterface * _broadphase;
	btCollisionConfiguration * _collisionConfiguration;
	btCollisionDispatcher * _dispatcher;
	btConstraintSolver * _solver;
	btDynamicsWorld * _world;

	btClock _clock;
	GameObjects _objects;
	DebugDrawer * _debugDrawer;

	btRigidBody * _pickedBody;
	btTypedConstraint * _pickConstraint;
	btScalar _oldPickingDist;

	CollisionPairs _pairsLastUpdate;  // collision event variables
};

int glutmain(int argc, char * argv[], int width, int height, char const * title, BulletOpenGLApplication * app);

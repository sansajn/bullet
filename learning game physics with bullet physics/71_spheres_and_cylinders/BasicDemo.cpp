#include "BasicDemo.h"
#include <bullet/btBulletDynamicsCommon.h>


BasicDemo::BasicDemo()
	: BulletOpenGLApplication{}
	, _applyForce{false}
	, _explosion{0}
	, _canExplode{true}
{}

void BasicDemo::InitializePhysics()
{
	_collisionConfiguration = new btDefaultCollisionConfiguration{};
	_dispatcher = new btCollisionDispatcher{_collisionConfiguration};
	_broadphase = new btDbvtBroadphase{};
	_solver = new btSequentialImpulseConstraintSolver{};
	_world = new btDiscreteDynamicsWorld{_dispatcher, _broadphase, _solver, _collisionConfiguration};
	CreateObjects();
}

void BasicDemo::ShutdownPhysics()
{
	delete _world;
	delete _solver;
	delete _broadphase;
	delete _dispatcher;
	delete _collisionConfiguration;
}

void BasicDemo::CreateObjects()
{
	// plane
	CreateGameObject(new btBoxShape{btVector3{1,50,50}}, 0, btVector3{0.2f, 0.6f, 0.6f}, btVector3{0.0f, 0.0f, 0.0f});

	// cube1
	_box = CreateGameObject(new btBoxShape(btVector3{1,1,1}), 1, btVector3{1, 0.2f, 0.2f}, btVector3{0, 10 ,0});

	// cube2
	CreateGameObject(new btBoxShape{btVector3{1,1,1}}, 1.0, btVector3{0.0f, 0.2f, 0.8f}, btVector3{1.25f, 20.0f, 0.0f});

	_trigger = new btCollisionObject();
	_trigger->setCollisionShape(new btBoxShape{btVector3{1, 0.2f, 1}});
	btTransform triggerTrans;
	triggerTrans.setIdentity();
	triggerTrans.setOrigin(btVector3{0, 1.5f, 0});
	_trigger->setWorldTransform(triggerTrans);
	_trigger->setCollisionFlags(btCollisionObject::CF_NO_CONTACT_RESPONSE);
	_world->addCollisionObject(_trigger);

	// create a yellow sphere
	CreateGameObject(new btSphereShape(1.0f), 1.0, btVector3(0.7f, 0.7f, 0.0f), btVector3(-5.0, 10.0f, 0.0f));

	// create a green cylinder
	CreateGameObject(new btCylinderShape(btVector3(1,1,1)), 1.0, btVector3(0.0f, 0.7f, 0.0f), btVector3(-2, 10.0f, 0.0f));
}

void BasicDemo::CollisionEvent(btRigidBody * body0, btRigidBody * body1)
{
	if (body0 == _box->GetRigidBody() && body1 == _trigger || body1 == _box->GetRigidBody() && body0 == _trigger)
		CreateGameObject(new btBoxShape{btVector3{2,2,2}}, 2, btVector3{0.3, 0.7, 0.3}, btVector3{5, 10, 0});

	if (body0 == _explosion || body1 == _explosion)
	{
		btRigidBody * other;
		body0 == _explosion ? other = (btRigidBody *)body1 : other = (btRigidBody *)body0;
		// wake the object up
		other->setActivationState(ACTIVE_TAG);
		// calculate the vector nbetween the object and the center of the explosion
		btVector3 dir = other->getWorldTransform().getOrigin() - _explosion->getWorldTransform().getOrigin();
		float dist = dir.length();
		float strength = EXPLOSION_STRENGTH;
		if (dist != 0.0)
			strength /= dist;
		dir.normalize();
		other->applyCentralImpulse(dir * strength);
	}
}

void BasicDemo::Keyboard(unsigned char key, int x, int y)
{
	BulletOpenGLApplication::Keyboard(key, x, y);
	switch (key)
	{
		case 'g': {  // force testing
			_applyForce = true;
			_box->GetRigidBody()->setActivationState(DISABLE_DEACTIVATION);
			break;
		}

		case 'e': {  // impulse testing
			if (_explosion || !_canExplode)
				break;
			_canExplode = false;
			_explosion = new btCollisionObject{};
			_explosion->setCollisionShape(new btSphereShape{3.0f});
			// get the position that we clicked
			RayResult result;
			Raycast(_cameraPosition, GetPickingRay(x,y), result, true);
			// create a transform from the hit point
			btTransform explodeTrans;
			explodeTrans.setIdentity();
			explodeTrans.setOrigin(result.hitPoint);
			_explosion->setWorldTransform(explodeTrans);
			// set the collision flag
			_explosion->setCollisionFlags(btCollisionObject::CF_NO_CONTACT_RESPONSE);
			// add the explosion trigger to our world
			_world->addCollisionObject(_explosion);
			break;
		}
	}  // switch (key)
}

void BasicDemo::KeyboardUp(unsigned char key, int x, int y)
{
	BulletOpenGLApplication::KeyboardUp(key, x, y);
	switch (key)
	{
		case 'g': {  // force testing
			_applyForce = false;
			_box->GetRigidBody()->forceActivationState(ACTIVE_TAG);
			break;
		}

		case 'e': {  // impulse testing
			_canExplode = true;
			break;
		}
	}  // switch (key)
}

void BasicDemo::UpdateScene(float dt)
{
	BulletOpenGLApplication::UpdateScene(dt);

	if (_applyForce)  // force testing
	{
		if (!_box)
			return;
		_box->GetRigidBody()->applyCentralImpulse(btVector3{0,20,0});
	}

	if (_explosion)  // impulse testing
	{
		_world->removeCollisionObject(_explosion);
		delete _explosion;
		_explosion = nullptr;
	}
}

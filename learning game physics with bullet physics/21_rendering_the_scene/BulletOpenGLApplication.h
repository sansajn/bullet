#pragma once
#include <GL/freeglut.h>
#include <bullet/LinearMath/btVector3.h>

class BulletOpenGLApplication
{
public:
	BulletOpenGLApplication();
	virtual ~BulletOpenGLApplication();
	
	// window events
	virtual void Initialize();
	virtual void Keyboard(unsigned char key, int x, int y);
	virtual void KeyboardUp(unsigned char key, int x, int y);
	virtual void Special(int key, int x, int y);
	virtual void SpecialUp(int key, int x, int y);
	virtual void Reshape(int w, int h);
	virtual void Idle();
	virtual void Mouse(int button, int state, int x, int y);
	virtual void PassiveMotion(int x, int y);
	virtual void Motion(int x, int y);
	virtual void Display();
	
	// camera functions
	void UpdateCamera();

protected:
	btVector3 _cameraPosition;
	btVector3 _cameraTarget;
	float _nearPlane;
	float _farPlane;
	btVector3 _upVector;

	int _screenWidth;
	int _screenHeight;
};

int glutmain(int argc, char * argv[], int width, int height, char const * title, BulletOpenGLApplication * app);

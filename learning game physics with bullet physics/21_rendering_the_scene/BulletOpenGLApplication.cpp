#include "BulletOpenGLApplication.h"


namespace detail {

BulletOpenGLApplication * g_pApp = nullptr;

void KeyboardCallback(unsigned char key, int x, int y);
void KeyboardUpCallback(unsigned char key, int x, int y);
void SpecialCallback(int key, int x, int y);
void SpecialUpCallback(int key, int x, int y);
void ReshapeCallback(int w, int h);
void IdleCallback();
void MouseCallback(int button, int state, int x, int y);
void PassiveMotionCallback(int x, int y);
void MotionCallback(int x, int y);
void DisplayCallback();

}  // detail

int glutmain(int argc, char * argv[], int width, int height, char const * title, BulletOpenGLApplication * app)
{
	detail::g_pApp = app;
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGBA|GLUT_DEPTH);
	glutInitWindowPosition(0, 0);
	glutInitWindowSize(width, height);
	glutCreateWindow(title);

	detail::g_pApp->Initialize();

	glutKeyboardFunc(detail::KeyboardCallback);
	glutKeyboardUpFunc(detail::KeyboardUpCallback);
	glutSpecialFunc(detail::SpecialCallback);
	glutSpecialUpFunc(detail::SpecialUpCallback);
	glutReshapeFunc(detail::ReshapeCallback);
	glutIdleFunc(detail::IdleCallback);
	glutMouseFunc(detail::MouseCallback);
	glutPassiveMotionFunc(detail::MotionCallback);
	glutMotionFunc(detail::MotionCallback);
	glutDisplayFunc(detail::DisplayCallback);

	detail::g_pApp->Idle();

	glutMainLoop();

	return 0;
}

BulletOpenGLApplication::BulletOpenGLApplication()
	: _cameraPosition{10.0f, 5.0f, 0.0f}
	, _cameraTarget{0.0f, 0.0f, 0.0f}
	, _upVector{0.0f, 1.0f, 0.0f}
	, _nearPlane{1.0f}
	, _farPlane{1000.0f}
	, _screenWidth{0}
	, _screenHeight{0}
{}

BulletOpenGLApplication::~BulletOpenGLApplication()
{}

void BulletOpenGLApplication::Initialize()
{
	glClearColor(0.6, 0.65, 0.85, 0);
}
	
void BulletOpenGLApplication::Keyboard(unsigned char key, int x, int y) {}
void BulletOpenGLApplication::KeyboardUp(unsigned char key, int x, int y) {}
void BulletOpenGLApplication::Special(int key, int x, int y) {}
void BulletOpenGLApplication::SpecialUp(int key, int x, int y) {}

void BulletOpenGLApplication::Reshape(int w, int h) 
{
	_screenWidth = w;
	_screenHeight = h;
	glViewport(0, 0, w, h);
	UpdateCamera();
}

void BulletOpenGLApplication::Idle() 
{
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	UpdateCamera();
	glutSwapBuffers();
}

void BulletOpenGLApplication::Mouse(int button, int state, int x, int y) {}
void BulletOpenGLApplication::PassiveMotion(int x, int y) {}
void BulletOpenGLApplication::Motion(int x, int y) {}
void BulletOpenGLApplication::Display() {}

void BulletOpenGLApplication::UpdateCamera()
{
	if (_screenWidth == 0 && _screenHeight == 0)
		return;

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	float aspectRatio = _screenWidth / (float)_screenHeight;
	glFrustum(-aspectRatio * _nearPlane, aspectRatio * _nearPlane, -_nearPlane, _nearPlane, _nearPlane, _farPlane);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(_cameraPosition[0], _cameraPosition[1], _cameraPosition[2], _cameraTarget[0], _cameraTarget[1], _cameraTarget[2], _upVector.getX(), _upVector.getY(), _upVector.getZ());
}

namespace detail {

void KeyboardCallback(unsigned char key, int x, int y) 
{
	g_pApp->Keyboard(key, x, y);
}

void KeyboardUpCallback(unsigned char key, int x, int y)
{
	g_pApp->KeyboardUp(key, x, y);
}

void SpecialCallback(int key, int x, int y)
{
	g_pApp->Special(key, x, y);
}

void SpecialUpCallback(int key, int x, int y)
{
	g_pApp->SpecialUp(key, x, y);
}

void ReshapeCallback(int w, int h)
{
	g_pApp->Reshape(w, h);
}

void IdleCallback()
{
	g_pApp->Idle();
}

void MouseCallback(int button, int state, int x, int y)
{
	g_pApp->Mouse(button, state, x, y);
}

void PassiveMotionCallback(int x, int y)
{
	g_pApp->PassiveMotion(x, y);
}

void MotionCallback(int x, int y)
{
	g_pApp->Motion(x, y);
}

void DisplayCallback()
{
	g_pApp->Display();
}

}  // detail

#include "BulletOpenGLApplication.h"
#include <bullet/BulletSoftBody/btSoftRigidDynamicsWorld.h>
#include <bullet/BulletSoftBody/btSoftBodyRigidBodyCollisionConfiguration.h>
#include <bullet/BulletSoftBody/btSoftBodyHelpers.h>

class SoftBodyDemo : public BulletOpenGLApplication
{
public:
	SoftBodyDemo();
	void InitializePhysics() override;
	void ShutdownPhysics() override;
	void RenderScene() override;
	void CreateObjects();

protected:
	btSoftRigidDynamicsWorld * _softBodyWorld;
	btSoftBodyWorldInfo _softBodyWorldInfo;
};
#include "DebugDrawer.h"
#include "BulletOpenGLApplication.h"

void DebugDrawer::drawContactPoint(btVector3 const & pointOnB, btVector3 const & normalOnB, 
	btScalar distance, int lifeTime, btVector3 const & color)
{
	btVector3 const startPoint = pointOnB;
	btVector3 const endPoint = startPoint + normalOnB * distance;
	drawLine(startPoint, endPoint, color);
}

void DebugDrawer::drawLine(btVector3 const & from, btVector3 const & to, btVector3 const & color)
{
	glBegin(GL_LINES);
		glColor3f(color.getX(), color.getY(), color.getZ());
		glVertex3f(from.getX(), from.getY(), from.getZ());
		glVertex3f(to.getX(), to.getY(), to.getZ());
	glEnd();
}

void DebugDrawer::ToggleDebugFlag(int flag)
{
	if (_debugMode & flag)
		_debugMode = _debugMode & (~flag);  // disable flag
	else
		_debugMode |= flag;  // enble flag
}

void DebugDrawer::drawTriangle(btVector3 const & a, btVector3 const & b, btVector3 const & c, btVector3 const & color, btScalar alpha) 
{
	// calculate the normal for the three points (a, b and c)
	btVector3 const n = btCross(b-a,c-a).normalized();
	glBegin(GL_TRIANGLES);    
		// render a triangle of the given color
		glColor4f(color.getX(), color.getY(), color.getZ(),alpha);
		glNormal3d(n.getX(),n.getY(),n.getZ());
		glVertex3d(a.getX(),a.getY(),a.getZ());
		glVertex3d(b.getX(),b.getY(),b.getZ());
		glVertex3d(c.getX(),c.getY(),c.getZ());
	glEnd();
}

#include "SoftBodyDemo.h"

SoftBodyDemo::SoftBodyDemo() : BulletOpenGLApplication{}
{}

void SoftBodyDemo::InitializePhysics()
{
	_collisionConfiguration = new btSoftBodyRigidBodyCollisionConfiguration();
	_dispatcher = new btCollisionDispatcher(_collisionConfiguration);
	_broadphase = new btDbvtBroadphase();
	_solver = new btSequentialImpulseConstraintSolver();
	_world = new btSoftRigidDynamicsWorld(_dispatcher, _broadphase, _solver, _collisionConfiguration);
	_softBodyWorld = (btSoftRigidDynamicsWorld*)_world;

	// initialize the world info for soft bodies
	_softBodyWorldInfo.m_dispatcher = _dispatcher;
	_softBodyWorldInfo.m_broadphase = _broadphase;
	_softBodyWorldInfo.m_sparsesdf.Initialize();

	CreateObjects();
}

void SoftBodyDemo::ShutdownPhysics() 
{
	delete _world;
	delete _solver;
	delete _broadphase;
	delete _dispatcher;
	delete _collisionConfiguration;
}

void SoftBodyDemo::CreateObjects() 
{
	// create a ground plane

	CreateGameObject(new btBoxShape(btVector3(1,50,50)), 0, btVector3(0.2f, 0.6f, 0.6f), btVector3(0.0f, 0.0f, 0.0f));

	// create a soft 'ball' with 128 sides and a radius of 3
	btSoftBody*  pSoftBody = btSoftBodyHelpers::CreateEllipsoid(_softBodyWorldInfo, btVector3(0,0,0),btVector3(3,3,3),128);

	// set the body's position
	pSoftBody->translate(btVector3(0,15,0));

	// set the 'volume conservation coefficient'
	pSoftBody->m_cfg.kVC = 0.5;

	// set the 'linear stiffness'
	pSoftBody->m_materials[0]->m_kLST = 0.5;

	// set the total mass of the soft body
	pSoftBody->setTotalMass(5);

	// tell the soft body to initialize and
	// attempt to maintain the current pose
	pSoftBody->setPose(true,false);

	// add the soft body to our world
	_softBodyWorld->addSoftBody(pSoftBody);
}

void SoftBodyDemo::RenderScene() 
{
	// call the base rendering code first
	BulletOpenGLApplication::RenderScene();
	// check the list of our world's soft bodies
	for (int i=0; i< _softBodyWorld->getSoftBodyArray().size(); i++) {
		// get the body
		btSoftBody * pBody = (btSoftBody*)_softBodyWorld->getSoftBodyArray()[i];
		// is it possible to render?
		if (_softBodyWorld->getDebugDrawer() && !(_softBodyWorld->getDebugDrawer()->getDebugMode() & (btIDebugDraw::DBG_DrawWireframe))) {
			// draw it
			btSoftBodyHelpers::Draw(pBody, _softBodyWorld->getDebugDrawer(), _softBodyWorld->getDrawFlags());
		}
	}
}

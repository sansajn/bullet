#include "BulletOpenGLApplication.h"
#include <bullet/btBulletDynamicsCommon.h>

class CollisionFilteringDemo : public BulletOpenGLApplication
{
public:
	CollisionFilteringDemo();
	void InitializePhysics() override;
	void ShutdownPhysics() override;
	void CreateObjects();
};
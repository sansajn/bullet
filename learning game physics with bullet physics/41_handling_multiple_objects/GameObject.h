#pragma once
#include <bullet/btBulletDynamicsCommon.h>
#include "OpenGLMotionState.h"

class GameObject
{
public:
	GameObject(btCollisionShape * shape, float mass, btVector3 const & color,
		btVector3 const & initialPosition = btVector3{0,0,0}, btQuaternion const & initialRotation = btQuaternion{0,0,1,1});

	~GameObject();

	btCollisionShape * GetShape() {return _shape;}
	btRigidBody * GetRigidBody() {return _body;}
	btMotionState * GetMotionState() {return _motionState;}

	void GetTransform(btScalar * transform)
	{
		if (_motionState)
			_motionState->GetWorldTransform(transform);
	}

	btVector3 GetColor() {return _color;}

protected:
	btCollisionShape * _shape;
	btRigidBody * _body;
	OpenGLMotionState * _motionState;
	btVector3 _color;
};

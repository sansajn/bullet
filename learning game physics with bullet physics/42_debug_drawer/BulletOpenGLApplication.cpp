#include "BulletOpenGLApplication.h"
#include <bullet/LinearMath/btMatrix3x3.h>
#include <bullet/LinearMath/btQuaternion.h>

constexpr float RADIANS_PER_DEGREE = 0.01745329f;
constexpr float CAMERA_STEP_SIZE = 5.0f;

namespace detail {

BulletOpenGLApplication * g_pApp = nullptr;

void KeyboardCallback(unsigned char key, int x, int y);
void KeyboardUpCallback(unsigned char key, int x, int y);
void SpecialCallback(int key, int x, int y);
void SpecialUpCallback(int key, int x, int y);
void ReshapeCallback(int w, int h);
void IdleCallback();
void MouseCallback(int button, int state, int x, int y);
void PassiveMotionCallback(int x, int y);
void MotionCallback(int x, int y);
void DisplayCallback();

}  // detail

int glutmain(int argc, char * argv[], int width, int height, char const * title, BulletOpenGLApplication * app)
{
	detail::g_pApp = app;
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGBA|GLUT_DEPTH);
	glutInitWindowPosition(0, 0);
	glutInitWindowSize(width, height);
	glutCreateWindow(title);

	detail::g_pApp->Initialize();

	glutKeyboardFunc(detail::KeyboardCallback);
	glutKeyboardUpFunc(detail::KeyboardUpCallback);
	glutSpecialFunc(detail::SpecialCallback);
	glutSpecialUpFunc(detail::SpecialUpCallback);
	glutReshapeFunc(detail::ReshapeCallback);
	glutIdleFunc(detail::IdleCallback);
	glutMouseFunc(detail::MouseCallback);
	glutPassiveMotionFunc(detail::MotionCallback);
	glutMotionFunc(detail::MotionCallback);
	glutDisplayFunc(detail::DisplayCallback);

	detail::g_pApp->Idle();

	glutMainLoop();

	return 0;
}

BulletOpenGLApplication::BulletOpenGLApplication()
	: _cameraPosition{10.0f, 5.0f, 0.0f}
	, _cameraTarget{0.0f, 0.0f, 0.0f}
	, _cameraDistance{15.0f}
	, _cameraPitch{20.0f}
	, _cameraYaw{0.0f}
	, _upVector{0.0f, 1.0f, 0.0f}
	, _nearPlane{1.0f}
	, _farPlane{1000.0f}
	, _screenWidth{0}
	, _screenHeight{0}
	, _broadphase{nullptr}
	, _collisionConfiguration{nullptr}
	, _dispatcher{nullptr}
	, _solver{nullptr}
	, _world{nullptr}
{}

BulletOpenGLApplication::~BulletOpenGLApplication()
{
	ShutdownPhysics();
}

void BulletOpenGLApplication::Initialize()
{
	GLfloat ambient[] = {0.2f, 0.2f, 0.2f, 1.0f};
	GLfloat diffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};
	GLfloat specular[] = {1.0f, 1.0f, 1.0f, 1.0f};
	GLfloat position[] = {5.0f, 10.0f, 1.0f, 0.0f};

	glLightfv(GL_LIGHT0, GL_AMBIENT, ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, specular);
	glLightfv(GL_LIGHT0, GL_POSITION, position);

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_COLOR_MATERIAL);

	glMaterialfv(GL_FRONT, GL_SPECULAR, specular);
	glMateriali(GL_FRONT, GL_SHININESS, 15);

	glShadeModel(GL_SMOOTH);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);

	glClearColor(0.6, 0.65, 0.85, 0);

	InitializePhysics();

	_debugDrawer = new DebugDrawer{};
	_debugDrawer->setDebugMode(0);
	_world->setDebugDrawer(_debugDrawer);
}
	
void BulletOpenGLApplication::Keyboard(unsigned char key, int x, int y) 
{
	switch (key)
	{
		case 'z': ZoomCamera(CAMERA_STEP_SIZE); break;
		case 'x': ZoomCamera(-CAMERA_STEP_SIZE); break;
		case 'w': _debugDrawer->ToggleDebugFlag(btIDebugDraw::DBG_DrawWireframe); break;
		case 'b': _debugDrawer->ToggleDebugFlag(btIDebugDraw::DBG_DrawAabb); break;
	}
}

void BulletOpenGLApplication::KeyboardUp(unsigned char key, int x, int y) {}

void BulletOpenGLApplication::Special(int key, int x, int y)
{
	switch (key)
	{
		// the arrow keys
		case GLUT_KEY_LEFT: RotateCamera(_cameraYaw, CAMERA_STEP_SIZE); break;
		case GLUT_KEY_RIGHT: RotateCamera(_cameraYaw, -CAMERA_STEP_SIZE); break;
		case GLUT_KEY_UP: RotateCamera(_cameraPitch, CAMERA_STEP_SIZE); break;
		case GLUT_KEY_DOWN: RotateCamera(_cameraPitch, -CAMERA_STEP_SIZE); break;
	}
}

void BulletOpenGLApplication::SpecialUp(int key, int x, int y) {}

void BulletOpenGLApplication::Reshape(int w, int h) 
{
	_screenWidth = w;
	_screenHeight = h;
	glViewport(0, 0, w, h);
	UpdateCamera();
}

void BulletOpenGLApplication::Idle() 
{
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	float dt = _clock.getTimeMilliseconds();
	_clock.reset();
	UpdateScene(dt/1000.0f);

	UpdateCamera();
	
	RenderScene();

	glutSwapBuffers();
}

void BulletOpenGLApplication::Mouse(int button, int state, int x, int y) {}
void BulletOpenGLApplication::PassiveMotion(int x, int y) {}
void BulletOpenGLApplication::Motion(int x, int y) {}
void BulletOpenGLApplication::Display() {}

void BulletOpenGLApplication::UpdateCamera()
{
	if (_screenWidth == 0 && _screenHeight == 0)
		return;

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	float aspectRatio = _screenWidth / (float)_screenHeight;
	glFrustum(-aspectRatio * _nearPlane, aspectRatio * _nearPlane, -_nearPlane, _nearPlane, _nearPlane, _farPlane);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	float pitch = _cameraPitch * RADIANS_PER_DEGREE;
	float yaw = _cameraYaw * RADIANS_PER_DEGREE;

	btQuaternion rotation(_upVector, yaw);

	btVector3 cameraPosition(0, 0, 0);
	cameraPosition[2] = -_cameraDistance;

	btVector3 forward(cameraPosition[0], cameraPosition[1], cameraPosition[2]);
	if (forward.length2() < SIMD_EPSILON)
		forward.setValue(1.f, 0.f, 0.f);

	btVector3 right = _upVector.cross(forward);

	btQuaternion roll(right, -pitch);

	cameraPosition = btMatrix3x3(rotation) * btMatrix3x3(roll) * cameraPosition;

	_cameraPosition[0] = cameraPosition.getX();
	_cameraPosition[1] = cameraPosition.getY();
	_cameraPosition[2] = cameraPosition.getZ();
	_cameraPosition += _cameraTarget;

	gluLookAt(_cameraPosition[0], _cameraPosition[1], _cameraPosition[2], _cameraTarget[0], _cameraTarget[1], _cameraTarget[2], _upVector.getX(), _upVector.getY(), _upVector.getZ());
}

void BulletOpenGLApplication::InitializePhysics()
{}

void BulletOpenGLApplication::ShutdownPhysics()
{}

void BulletOpenGLApplication::DrawBox(btVector3 const & halfSize)
{
	float halfWidth = halfSize.x();
	float halfHeight = halfSize.y();
	float halfDepth = halfSize.z();

	btVector3 vertices[8]= {	
		btVector3(halfWidth,halfHeight,halfDepth),
		btVector3(-halfWidth,halfHeight,halfDepth),
		btVector3(halfWidth,-halfHeight,halfDepth),	
		btVector3(-halfWidth,-halfHeight,halfDepth),	
		btVector3(halfWidth,halfHeight,-halfDepth),
		btVector3(-halfWidth,halfHeight,-halfDepth),	
		btVector3(halfWidth,-halfHeight,-halfDepth),	
		btVector3(-halfWidth,-halfHeight,-halfDepth)
	};
	
	static int indices[36] = {
		0,1,2,
		3,2,1,
		4,0,6,
		6,0,2,
		5,1,4,
		4,1,0,
		7,3,1,
		7,1,5,
		5,4,7,
		7,4,6,
		7,2,3,
		7,6,2};
	
	glBegin (GL_TRIANGLES);
	
		for (int i = 0; i < 36; i += 3) {
			const btVector3 &vert1 = vertices[indices[i]];
			const btVector3 &vert2 = vertices[indices[i+1]];
			const btVector3 &vert3 = vertices[indices[i+2]];
	
			btVector3 normal = (vert3-vert1).cross(vert2-vert1);
			normal.normalize ();
	
			glNormal3f(normal.getX(),normal.getY(),normal.getZ());
	
			glVertex3f (vert1.x(), vert1.y(), vert1.z());
			glVertex3f (vert2.x(), vert2.y(), vert2.z());
			glVertex3f (vert3.x(), vert3.y(), vert3.z());
		}
	
	glEnd();
}

void BulletOpenGLApplication::RotateCamera(float & angle, float value)
{
	angle -= value;
	if (angle < 0)
		angle += 360;
	if (angle >= 360)
		angle -= 360;
	UpdateCamera();
}

void BulletOpenGLApplication::ZoomCamera(float distance)
{
	_cameraDistance -= distance;
	if (_cameraDistance < 0.1f) 
		_cameraDistance = 0.1f;
	UpdateCamera();
}

void BulletOpenGLApplication::RenderScene()
{
	btScalar transform[16];
	for (GameObject * o : _objects)
	{
		o->GetTransform(transform);
		DrawShape(transform, o->GetShape(), o->GetColor());
	}
	_world->debugDrawWorld();
}

void BulletOpenGLApplication::UpdateScene(float dt)
{
	if (_world)
		_world->stepSimulation(dt);
}

void BulletOpenGLApplication::DrawShape(btScalar * transform, btCollisionShape const * shape, btVector3 const & color)
{
	glColor3f(color.x(), color.y(), color.z());

	glPushMatrix();
	glMultMatrixf(transform);

	switch (shape->getShapeType())
	{
		case BOX_SHAPE_PROXYTYPE:
		{
			btBoxShape const * box = static_cast<btBoxShape const *>(shape);
			btVector3 halfSize = box->getHalfExtentsWithMargin();
			DrawBox(halfSize);
			break;
		}

		default:
			break;
	}

	glPopMatrix();
}

GameObject * BulletOpenGLApplication::CreateGameObject(btCollisionShape * shape, float mass,
	btVector3 const & color, btVector3 const & initialPosition, btQuaternion const & initialRotation)
{
	GameObject * obj = new GameObject{shape, mass, color, initialPosition, initialRotation};
	_objects.push_back(obj);
	if (_world)
		_world->addRigidBody(obj->GetRigidBody());
	return obj;
}


namespace detail {

void KeyboardCallback(unsigned char key, int x, int y) 
{
	g_pApp->Keyboard(key, x, y);
}

void KeyboardUpCallback(unsigned char key, int x, int y)
{
	g_pApp->KeyboardUp(key, x, y);
}

void SpecialCallback(int key, int x, int y)
{
	g_pApp->Special(key, x, y);
}

void SpecialUpCallback(int key, int x, int y)
{
	g_pApp->SpecialUp(key, x, y);
}

void ReshapeCallback(int w, int h)
{
	g_pApp->Reshape(w, h);
}

void IdleCallback()
{
	g_pApp->Idle();
}

void MouseCallback(int button, int state, int x, int y)
{
	g_pApp->Mouse(button, state, x, y);
}

void PassiveMotionCallback(int x, int y)
{
	g_pApp->PassiveMotion(x, y);
}

void MotionCallback(int x, int y)
{
	g_pApp->Motion(x, y);
}

void DisplayCallback()
{
	g_pApp->Display();
}

}  // detail

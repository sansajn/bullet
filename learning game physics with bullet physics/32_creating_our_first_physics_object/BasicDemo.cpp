#include "BasicDemo.h"
#include <bullet/btBulletDynamicsCommon.h>

void BasicDemo::InitializePhysics()
{
	_collisionConfiguration = new btDefaultCollisionConfiguration{};
	_dispatcher = new btCollisionDispatcher{_collisionConfiguration};
	_broadphase = new btDbvtBroadphase{};
	_solver = new btSequentialImpulseConstraintSolver{};
	_world = new btDiscreteDynamicsWorld{_dispatcher, _broadphase, _solver, _collisionConfiguration};
	CreateObjects();
}

void BasicDemo::ShutdownPhysics()
{
	delete _world;
	delete _solver;
	delete _broadphase;
	delete _dispatcher;
	delete _collisionConfiguration;
}

void BasicDemo::CreateObjects()
{
	btBoxShape * boxShape = new btBoxShape{btVector3{1.0f, 1.0f, 1.0f}};

	btTransform transform;
	transform.setIdentity();
	transform.setOrigin(btVector3{0.0f, 0.0f, 0.0f});
	_motionState = new OpenGLMotionState{transform};

	btRigidBody::btRigidBodyConstructionInfo rbInfo{1.0f, _motionState, boxShape};
	btRigidBody * rigidBody = new btRigidBody{rbInfo};
	_world->addRigidBody(rigidBody);
}

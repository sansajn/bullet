#include "BasicDemo.h"
#include <bullet/btBulletDynamicsCommon.h>


void BasicDemo::InitializePhysics()
{
	_collisionConfiguration = new btDefaultCollisionConfiguration{};
	_dispatcher = new btCollisionDispatcher{_collisionConfiguration};
	_broadphase = new btDbvtBroadphase{};
	_solver = new btSequentialImpulseConstraintSolver{};
	_world = new btDiscreteDynamicsWorld{_dispatcher, _broadphase, _solver, _collisionConfiguration};
	CreateObjects();
}

void BasicDemo::ShutdownPhysics()
{
	delete _world;
	delete _solver;
	delete _broadphase;
	delete _dispatcher;
	delete _collisionConfiguration;
}

void BasicDemo::CreateObjects()
{
	// plane
	CreateGameObject(new btBoxShape{btVector3{1,50,50}}, 0, btVector3{0.2f, 0.6f, 0.6f}, btVector3{0.0f, 0.0f, 0.0f});

	// cube1
	CreateGameObject(new btBoxShape{btVector3{1,1,1}}, 1.0, btVector3{1.0f, 0.2f, 0.2f}, btVector3{0.0f, 10.0f, 0.0f});

	// cube2
	CreateGameObject(new btBoxShape{btVector3{1,1,1}}, 1.0, btVector3{0.0f, 0.2f, 0.8f}, btVector3{1.25f, 20.0f, 0.0f});
}

#include "GameObject.h"

GameObject::GameObject(btCollisionShape * shape, float mass, btVector3 const & color,
	btVector3 const & initialPosition, btQuaternion const & initialRotation)
{
	_shape = shape;
	_color = color;

	btTransform transform;
	transform.setIdentity();
	transform.setOrigin(initialPosition);
	transform.setRotation(initialRotation);
	_motionState = new OpenGLMotionState(transform);

	btVector3 localInertia{0,0,0};
	if (mass != 0.0f)
		_shape->calculateLocalInertia(mass, localInertia);

	btRigidBody::btRigidBodyConstructionInfo cInfo{mass, _motionState, shape, localInertia};

	_body = new btRigidBody{cInfo};
}

GameObject::~GameObject()
{
	delete _body;
	delete _motionState;
	delete _shape;
}

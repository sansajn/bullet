#include "BasicDemo.h"
#include <bullet/btBulletDynamicsCommon.h>

void BasicDemo::InitializePhysics()
{
	_collisionConfiguration = new btDefaultCollisionConfiguration{};
	_dispatcher = new btCollisionDispatcher{_collisionConfiguration};
	_broadphase = new btDbvtBroadphase{};
	_solver = new btSequentialImpulseConstraintSolver{};
	_world = new btDiscreteDynamicsWorld{_dispatcher, _broadphase, _solver, _collisionConfiguration};
}

void BasicDemo::ShutdownPhysics()
{
	delete _world;
	delete _solver;
	delete _broadphase;
	delete _dispatcher;
	delete _collisionConfiguration;
}

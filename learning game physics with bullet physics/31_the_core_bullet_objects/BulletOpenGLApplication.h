#pragma once
#include <GL/freeglut.h>
#include <bullet/BulletDynamics/Dynamics/btDynamicsWorld.h>

class BulletOpenGLApplication
{
public:
	BulletOpenGLApplication();
	virtual ~BulletOpenGLApplication();
	
	// window events
	virtual void Initialize();
	virtual void Keyboard(unsigned char key, int x, int y);
	virtual void KeyboardUp(unsigned char key, int x, int y);
	virtual void Special(int key, int x, int y);
	virtual void SpecialUp(int key, int x, int y);
	virtual void Reshape(int w, int h);
	virtual void Idle();
	virtual void Mouse(int button, int state, int x, int y);
	virtual void PassiveMotion(int x, int y);
	virtual void Motion(int x, int y);
	virtual void Display();

	// physics functions
	virtual void InitializePhysics();
	virtual void ShutdownPhysics();
	
	// camera functions
	void UpdateCamera();
	void RotateCamera(float & angle, float value);
	void ZoomCamera(float distance);

	// drawing functions
	void DrawBox(btVector3 const & halfSize, btVector3 const & color = btVector3(1.0f, 1.0f, 1.0f));

protected:
	btVector3 _cameraPosition;
	btVector3 _cameraTarget;
	float _nearPlane;
	float _farPlane;
	btVector3 _upVector;
	float _cameraDistance;
	float _cameraPitch;
	float _cameraYaw;

	int _screenWidth;
	int _screenHeight;

	btBroadphaseInterface * _broadphase;
	btCollisionConfiguration * _collisionConfiguration;
	btCollisionDispatcher * _dispatcher;
	btConstraintSolver * _solver;
	btDynamicsWorld * _world;
};

int glutmain(int argc, char * argv[], int width, int height, char const * title, BulletOpenGLApplication * app);

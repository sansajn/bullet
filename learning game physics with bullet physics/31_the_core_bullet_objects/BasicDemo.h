#pragma once
#include "BulletOpenGLApplication.h"

class BasicDemo : public BulletOpenGLApplication
{
public:
	void InitializePhysics() override;
	void ShutdownPhysics() override;
};
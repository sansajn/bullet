// fyzika zasuvacích dverí s hry wolfenstein 3d
#include <vector>
#include "gl/glfw3_window.hpp"
#include "gl/glfw3_user_input.hpp"
#include "gl/controllers.hpp"
#include "gl/free_camera.hpp"
//#include "gl/scene_object.hpp"
#include "gles2/label_gles2.hpp"
#include "physics/physics.hpp"
#include "geometry/utility.hpp"
#include <iostream>

using std::vector;
using glm::vec3;
using glm::mat3;
using glm::ivec2;
using glm::radians;
using geom::right;
using geom::forward;
using gl::camera;

using namespace phys;

std::string const font_path = "/usr/share/fonts/truetype/freefont/FreeMono.ttf";

enum collision_groups  // bullet use groups up to 32
{
	colgroup_wall = 64,
	colgroup_door = 128
};

// game mechanics

class state_machine  //!< implmentacia konecneho stavoveho stroja pre herne objekty
{
public:
	struct state  //!< stav stavoveho stroja
	{
		virtual ~state() {}
		virtual void enter() {}
		virtual void update(float dt) {}
		virtual void exit() {}
		state_machine * owner = nullptr;
	};

	virtual ~state_machine() {}
	void init(state * s);
	virtual void update(float dt);
	virtual void change_state(state * s);
	virtual void change_state(int state_type_id) {}
	state * current() const {return _cur;}

private:
	state * _cur = nullptr;
};

class fps_move  //!< wsad move in xz plane
{
public:
	fps_move() {}
	void init(ui::glfw3::user_input * in, btRigidBody * body, float velocity = 2);
	void input(float dt);
	void controls(char forward, char backward, char left, char right);

private:
	enum class key {forward, backward, left, right, key_count};
	btRigidBody * _body;
	ui::glfw3::user_input * _in;
	float _velocity;
	char _controls[(int)key::key_count+1];
};

class fps_look
{
public:
	fps_look() {}
	void init(ui::glfw_pool_window * window, btRigidBody * body, float velocity = 1);
	void input(float dt);

private:
	ui::glfw_pool_window * _window;
	btRigidBody * _body;
	float _velocity;
	bool _enabled = false;
};

class fps_player
{
public:
	fps_player() {}
	void init(glm::vec3 const & position, float fovy, float aspect_ratio, float near, float far, ui::glfw_pool_window * window);
	gl::camera & get_camera() {return _cam;}
	void link_with(rigid_body_world & world);
	btRigidBody * body() const {return _collision.native();}
	void input(float dt);
	void update(float dt);
//	render();

private:
	gl::camera _cam;
	body_object _collision;
	ui::glfw_pool_window * _window;
	fps_look _look;
	fps_move _move;
};


enum class door_states {closed, openning, open, closing};

class door;

struct door_closed : public state_machine::state
{};

class door_openning : public state_machine::state
{
public:
	door_openning() {}  //!< makes uninitialized object
	door_openning(door * d) : _door{d} {}
	void enter() override;
	void update(float dt) override;
	void exit() override;

private:
	door * _door = nullptr;
	float DURATION = 1.0f;
};

class door_open : public state_machine::state
{
public:
	door_open() {}
	door_open(door * d) : _door{d} {}
	void update(float dt) override;

private:
	float _t = 0;
	door * _door = nullptr;
	float DURATION = 3.0f;
};

class door_closing : public state_machine::state
{
public:
	door_closing() {}
	door_closing(door * d) : _door{d} {}
	void enter() override;
	void update(float dt) override;
	void exit() override;

private:
	door * _door = nullptr;
	float DURATION = 1.0f;
};

class door_state_machine : public state_machine
{
public:
	door_state_machine() {}
	void init(door * d);
	void enter_openning_sequence();

private:
	void change_state(int state_type_id) override;

	door_closed _closed;
	door_openning _openning;
	door_open _open;
	door_closing _closing;

	door * _door;
};

class door
{
public:
	door() {}
	void init(btVector3 const & position);
	void update(float dt);
	void open();
	bool can_close() const {return true;}
	void link_with(rigid_body_world & world);  // const

	// low level
	body_object & collision() {return _collision;}
	btVector3 open_position() const;
	btVector3 const & closed_position() const;

private:
	body_object _collision;
	door_state_machine _state;
	btVector3 _closed_pos;
};

class door_scene : public ui::glfw_pool_window
{
public:
	using base = ui::glfw_pool_window;
	door_scene();
	void input(float dt) override;
	void update(float dt) override;
	void display() override;

private:
	gl::free_camera<door_scene> _view;
//	axis_object _axis;
	gles2::ui::label<door_scene> _info;

	// phys
	rigid_body_world _world;
	body_object _ground;
	vector<body_object> _walls;

	// game objects (od scene objektou sa lisia fyzikou)
	door _door;
	fps_player _player;

	// mechanics
	bool _unlock_camera = false;
};

door_scene::door_scene()
	: base{parameters{}.name("wolfenstein 3d door physics")}
	, _view{radians(70.0f), aspect_ratio(), 0.01, 1000, *this}
{
	_view.get_camera().position = vec3{-2, 3, 5};
	_view.get_camera().look_at(vec3{0,0,0});

	_info.init(0, 0, *this);
	_info.font(font_path, 16);

	_ground = body_object{make_box_shape(btVector3{10, .1, 10}), 0, btVector3{0, -.1, 0}};
	_world.link(_ground);

	// walls
	auto wall_shape = make_box_shape(btVector3{1.5, .5, .5});
	_walls.emplace_back(wall_shape, 0, btVector3{-2, .5, 0});
	_walls.emplace_back(wall_shape, 0, btVector3{2, .5, 0});
	for (auto & w : _walls)  // steny nekoliduju s dverami
		_world.native()->addRigidBody(w.native(), colgroup_wall, ~colgroup_door);

	_door.init(btVector3{0, .5, 0});
	_door.link_with(_world);

	_player.init(vec3{0, 0, 1.5}, radians(70.0f), aspect_ratio(), 0.01, 1000, this);
	_player.link_with(_world);

	glClearColor(0,0,0,1);
}

void door_scene::display()
{
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);
//	_axis.render(_view.get_camera().view_projection());
	_info.render();
	_world.debug_render(_view.get_camera().view_projection());
	base::display();
}

void door_scene::update(float dt)
{
	base::update(dt);
	_world.update(dt);
	_player.update(dt);
	_door.update(dt);
}

void door_scene::input(float dt)
{
	if (in().key_up(' '))
		_door.open();

	if (in().key_up('U'))  // unlock camera
	{
		_unlock_camera = true;
		_info.text("camera unlocked");
	}

	if (_unlock_camera && in().key_up(27))  // esc - lock camera
	{
		_unlock_camera = false;
		_info.text("");
	}

	if (_unlock_camera)
		_view.input(dt);
	else
		_player.input(dt);

	base::input(dt);
}


void door::update(float dt)
{
	_state.update(dt);
}

btVector3 door::open_position() const
{
	return _closed_pos - btVector3{.9,0,0};
}

btVector3 const & door::closed_position() const
{
	return _closed_pos;
}

void door::open()
{
	_state.enter_openning_sequence();
}

void door::init(btVector3 const & position)
{
	_collision = body_object{make_box_shape(btVector3{.5, .5, .1}), 500, position};
	_collision.native()->setLinearFactor(btVector3{0,0,0});  // closed doors don't move
	_collision.native()->setAngularFactor(0);
	_state.init(this);
	_closed_pos = position;
}

void door::link_with(rigid_body_world & world)
{
	world.native()->addRigidBody(_collision.native(), colgroup_door, ~colgroup_wall);  // dvere nekoliduju zo stenami
}


void door_openning::enter()
{
	btRigidBody * body = _door->collision().native();
	body->setLinearVelocity(btVector3{-1.0f/DURATION, 0, 0});
	if (!body->isActive())
		body->activate();
}

void door_openning::update(float dt)
{
	if (_door->collision().position().x() <= _door->open_position().x())
		owner->change_state((int)door_states::open);
}

void door_openning::exit()
{
	_door->collision().native()->setLinearVelocity(btVector3{0,0,0});  // zastav pohyb
}

void door_open::update(float dt)
{
	_t += dt;
	if (_t >= DURATION && _door->can_close())
		owner->change_state((int)door_states::closing);
}

void door_closing::enter()
{
	btRigidBody * body = _door->collision().native();
	body->setLinearVelocity(btVector3{1.0f/DURATION, 0, 0});
	if (!body->isActive())
		body->activate();
}

void door_closing::update(float dt)
{
	if (_door->collision().position().x() >= _door->closed_position().x())
		owner->change_state((int)door_states::closed);
}

void door_closing::exit()
{
	btRigidBody * body = _door->collision().native();
	body->setLinearVelocity(btVector3{0,0,0});  // zastav pohyb
	body->setLinearFactor(btVector3{0,0,0});
}


void door_state_machine::init(door * d)
{
	_door = d;
	state_machine::init(&_closed);
}

void door_state_machine::enter_openning_sequence()
{
	if (current() == &_closed)
		change_state((int)door_states::openning);
}

void door_state_machine::change_state(int state_type_id)
{
	switch (state_type_id)
	{
		case (int)door_states::closed:
			_closed = door_closed{};
			state_machine::change_state(&_closed);
			break;

		case (int)door_states::openning:
			_openning = door_openning{_door};
			state_machine::change_state(&_openning);
			break;

		case (int)door_states::open:
			_open = door_open{_door};
			state_machine::change_state(&_open);
			break;

		case (int)door_states::closing:
			_closing = door_closing{_door};
			state_machine::change_state(&_closing);
			break;

		default:
			throw std::logic_error{"bad cast"};
	}
}

void state_machine::init(state * s)
{
	_cur = s;
	_cur->owner = this;
	_cur->enter();
}

void state_machine::change_state(state * s)
{
	_cur->exit();
	_cur = s;
	_cur->owner = this;
	_cur->enter();
}

void state_machine::update(float dt)
{
	_cur->update(dt);
}


void fps_move::init(ui::glfw3::user_input * in, btRigidBody * body, float velocity)
{
	assert(in && body && "invalid pointer");
	_in = in;
	_body = body;
	_velocity = velocity;
	strcpy(_controls, "WSAD");  // default controls
}

void fps_move::input(float dt)
{
	vec3 vel{0,0,0};

	if (_in->any_of_key(_controls))
	{
		mat3 r = mat3_cast(glm_cast(_body->getOrientation()));
		vec3 right_dir = right(r);
		vec3 forward_dir = forward(r);

		if (_in->key(_controls[(int)key::forward]))
			vel -= forward_dir;
		if (_in->key(_controls[(int)key::backward]))
			vel += forward_dir;
		if (_in->key(_controls[(int)key::left]))
			vel -= right_dir;
		if (_in->key(_controls[(int)key::right]))
			vel += right_dir;

		vel.y = 0;  // chcem sa pohybovat iba po rovine
		vel *= _velocity;
	}

	_body->setLinearVelocity(bullet_cast(vel));
}

void fps_look::init(ui::glfw_pool_window * window, btRigidBody * body, float velocity)
{
	_window = window;
	_body = body;
	_velocity = velocity;
}

void fps_look::input(float dt)
{
	using glm::vec2;
	using glm::vec3;

	assert(_window);

	// rotation
	float angular_movement = 0.1f;  // rad/s
	vec2 r = _window->in().mouse_position();

	if (r.x != 0.0f)
	{
		float angle = angular_movement * r.x * dt;
		btTransform transf = _body->getCenterOfMassTransform();
		btQuaternion r = btQuaternion{btVector3{0,1,0}, -angle} * transf.getRotation();
		_body->setCenterOfMassTransform(btTransform{r, transf.getOrigin()});
	}

	if (r.y != 0.0f)
	{
		float angle = angular_movement * r.y * dt;
		btTransform transf = _body->getCenterOfMassTransform();
		btVector3 right_dir = transf.getBasis().getColumn(0);
		btQuaternion r = btQuaternion{right_dir, -angle} * transf.getRotation();
		_body->setCenterOfMassTransform(btTransform{r, transf.getOrigin()});
	}
}

void fps_player::init(vec3 const & position, float fovy, float aspect_ratio, float near, float far, ui::glfw_pool_window * window)
{
	_cam = camera{fovy, aspect_ratio, near, far};

	float const mass = 90.0f;
	_collision = body_object{make_box_shape(btVector3{0.25, 0.25, 0.25}), mass, btVector3{0, 0.5, 0} + bullet_cast(position)};
	_collision.native()->setAngularFactor(0);  // nechcem aby sa hrac otacal pri kolizii
	_collision.native()->setActivationState(DISABLE_DEACTIVATION);

	_window = window;
	_look.init(_window, _collision.native());
	_move.init(&_window->in(), _collision.native());
}

void fps_player::input(float dt)
{
	_look.input(dt);
	_move.input(dt);
}

void fps_player::link_with(rigid_body_world & world)
{
	world.link(_collision);
	_collision.native()->setGravity(btVector3{0,0,0});  // turn off object gravity
}

void fps_player::update(float dt)
{
	_cam.position = glm_cast(_collision.position());
	_cam.rotation = glm_cast(_collision.rotation());
}


int main(int argc, char * argv[])
{
	door_scene w;
	w.start();
	return 0;
}

# physics

Použtie bullet fyzikálneho engine-u.

## physics:ukážky

**maze**: hráč prechádzajúci bludiskom, kolízie stien a hráča

![](wiki/maze_gles2_debug.png "maze sample")

ovládanie: myš kamera a `WSAD` pohyb hráča po bludisku. 


**shoot_wall**: vrhanie kociek voci stene (kolízie vo fizikálnom svete)

![](wiki/shoot_wall_gles2.png "shoot wall sample")

ovládanie: `WSAD` pohyb v rovinne xy a klávesa space hod kocky


**player**: fps like player

**move_static**: pohyb statikeho objektu (neuplna ukážka)

**raycast**: použitie techniky sledovania lúča k určeniu polohy objektu


**door**: fyzika zasuvacích dverí s hry wolfenstain 3d

![](wiki/door_gles2.png "door sample")

ovládanie: `space` otvorí dvere, `U`/`esc` unlock/lock camera, `WSAD` pohyb hráča.


## physics:projekty

**shoot_wall**: projekt odvodený od ukážky shoot_wall adaptovanaý pod gles2


## physics:štruktúra

{demos}

Ukážky s bullet-u.


{learning game physics with bullet physics}

Ukážky s knihy s rovnakým názvom 

	https://www.packtpub.com/game-development/learning-game-physics-bullet-physics-and-opengl


{video_tutorial}

...


## physics:inštalácia

Inštalácia

kubuntu 18.04

libassimp-dev 4.1.0
libbullet-dev 2.87




// pouzitie techniky sledovania luca k dentifikacii objektu
#include <glm/glm.hpp>
#include "gl/window.hpp"
#include "gl/scene_object.hpp"
#include "gl/controllers.hpp"
#include "gl/label.hpp"
#include "physics/physics.hpp"
#include <iostream>

using glm::vec3;
using glm::vec2;
using glm::ivec2;
using glm::vec4;
using glm::radians;

using namespace phys;

std::string const font_path = "/usr/share/fonts/truetype/freefont/FreeMono.ttf";

class scene_window : public ui::glut_pool_window
{
public:
	using base = ui::glut_pool_window;

	scene_window();
	void input(float dt) override;
	void update(float dt) override;
	void display() override;

private:
	axis_object _axis;
	gl::free_camera<scene_window> _cam;
	ui::label<scene_window> _object_label;
	bool _on_object = false;
	ivec2 _last_cursor_pos;

	// phys
	rigid_body_world _world;
	body_object _cube;
	body_object _sphere;
	body_object _cylinder;
	body_object _ground;
};

scene_window::scene_window()
	: _cam{radians(70.0f), aspect_ratio(), 0.01, 1000, *this}
{
	_cam.get_camera().position = vec3{3, 3, 4};
	_cam.get_camera().look_at(vec3{0,0,0});

	_object_label.init(0, 0, *this);
	_object_label.font(font_path, 16);
	_object_label.hide();

	_cube = body_object{make_box_shape(btVector3{.5, .5, .5}), 1, btVector3{1.5, .5, -.5}};
	_sphere = body_object{make_sphere_shape(.5), 1};
	_cylinder = body_object{make_cylinder_shape(.5, 1), 1, btVector3{-1, .5, 2}};
	_ground = body_object{make_box_shape(btVector3{10, .1, 10}), 0, btVector3{0, -.1, 0}};

	_world.link(_cube);
	_world.link(_sphere);
	_world.link(_cylinder);
	_world.link(_ground);

	_last_cursor_pos = in.mouse_position();

	glClearColor(0,0,0,1);
}

void scene_window::display()
{
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);
	_axis.render(_cam.get_camera().view_projection());
	_world.debug_render(_cam.get_camera().view_projection());
	_object_label.render();
	base::display();
}

void scene_window::input(float dt)
{
	_cam.input(dt);

	if (_last_cursor_pos != in.mouse_position())
	{
		// identifikuj objekt
		btVector3 from = bullet_cast(_cam.get_camera().position);
		btVector3 to = from + bullet_cast(-10.0f * _cam.get_camera().forward()); // TODO: vypocet to zaloz na far-plane
		btCollisionWorld::ClosestRayResultCallback rayres{from, to};  // TODO: preco 2x from a to ?
		_world.native()->rayTest(from, to, rayres);

		// vytvor popisok objektu
		if (rayres.hasHit())
		{
			std::string label;
			btCollisionObject const * obj = rayres.m_collisionObject;
			if (obj == _cube.native())
				label = "cube";
			else if (obj == _sphere.native())
				label = "sphere";
			else if (obj == _cylinder.native())
				label = "cylinder";
			else if (obj == _ground.native())
				label = "ground";
			_object_label.text(label);

			_object_label.show();
		}
		else
			_object_label.hide();  // na nic neukazuje, skovaj popisok

		_last_cursor_pos = in.mouse_position();
	}

	base::input(dt);
}

void scene_window::update(float dt)
{
	base::update(dt);
	_world.update(dt);
}

int main(int argc, char * argv[])
{
	scene_window w;
	w.start();
	return 0;
}

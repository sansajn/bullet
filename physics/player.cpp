// fps like ovladanie hraca
#include <string>
#include "window.hpp"
#include "controllers.hpp"
#include "mesh.hpp"
#include "program.hpp"
#include "physics/physics.hpp"
#include "scene_object.hpp"
#include "geometry/utility.hpp"

using std::string;
using glm::vec3;
using glm::ivec2;
using glm::mat3;
using glm::radians;
using gl::mesh;
using gl::make_plane_xz;
using gl::free_camera;
using gl::camera;
using geom::right;
using geom::forward;
using ui::glut_pool_window;

using namespace phys;

string program_path = "shaders/shaded.glsl";

class fps_move  //!< wsad move in xz plane
{
public:
	fps_move() {}
	void init(glut_pool_window::user_input * in, btRigidBody * body, float velocity = 2);
	void input(float dt);
	void controls(char forward, char backward, char left, char right);

private:
	enum class key {forward, backward, left, right, key_count};
	btRigidBody * _body;
	glut_pool_window::user_input * _in;
	float _velocity;
	char _controls[(int)key::key_count+1];
};

void fps_move::init(glut_pool_window::user_input * in, btRigidBody * body, float velocity)
{
	assert(in && body && "invalid pointer");
	_in = in;
	_body = body;
	_velocity = velocity;
	strcpy(_controls, "wsad");  // default controls
}

void fps_move::input(float dt)
{
	vec3 vel{0,0,0};

	if (_in->one_of_key(_controls))
	{
		mat3 r = mat3_cast(glm_cast(_body->getOrientation()));
		vec3 right_dir = right(r);
		vec3 forward_dir = forward(r);

		if (_in->key(_controls[(int)key::forward]))
			vel -= forward_dir;
		if (_in->key(_controls[(int)key::backward]))
			vel += forward_dir;
		if (_in->key(_controls[(int)key::left]))
			vel -= right_dir;
		if (_in->key(_controls[(int)key::right]))
			vel += right_dir;

		vel.y = 0;  // chcem sa pohybovat iba po rovine
		vel *= _velocity;
	}

	_body->setLinearVelocity(bullet_cast(vel));
}

class fps_look
{
public:
	fps_look() {}
	void init(glut_pool_window * window, btRigidBody * body, float velocity = 1);
	void input(float dt);

private:
	glut_pool_window * _window;
	btRigidBody * _body;
	float _velocity;
	bool _enabled = false;
};

void fps_look::init(glut_pool_window * window, btRigidBody * body, float velocity)
{
	_window = window;
	_body = body;
	_velocity = velocity;
}

void fps_look::input(float dt)
{
	ivec2 screen_center = _window->center();

	if (_window->in.mouse(glut_pool_window::button::left) && !_enabled)
	{
		_enabled = true;
		glutSetCursor(GLUT_CURSOR_NONE);
		glutWarpPointer(screen_center.x, screen_center.y);
		return;
	}

	if (_window->in.key_up(27) && _enabled)  // esc
	{
		_enabled = false;
		glutSetCursor(GLUT_CURSOR_INHERIT);
	}

	if (!_enabled)
		return;

	float const angular_movement = 0.1f;
	ivec2 delta = _window->in.mouse_position() - screen_center;

	if (delta.x != 0)
	{
		float angle = radians(angular_movement * delta.x);
		btTransform transf = _body->getCenterOfMassTransform();
		btQuaternion r = btQuaternion{btVector3{0,1,0}, -angle} * transf.getRotation();
		_body->setCenterOfMassTransform(btTransform{r, transf.getOrigin()});
	}

	if (delta.y != 0)
	{
		float angle = radians(angular_movement * delta.y);
		btTransform transf = _body->getCenterOfMassTransform();
		btVector3 right_dir = transf.getBasis().getColumn(0);
		btQuaternion r = btQuaternion{right_dir, -angle} * transf.getRotation();
		_body->setCenterOfMassTransform(btTransform{r, transf.getOrigin()});
	}

	if (delta.x != 0 || delta.y != 0)
		glutWarpPointer(screen_center.x, screen_center.y);
}

class player  // fps_player
{
public:
	player() {}
	void init(vec3 const & position, float fovy, float aspect_ratio, float near, float far, glut_pool_window * window);
	camera & get_camera() {return _cam;}
	void link_with(rigid_body_world & world);
	btRigidBody * body() const {return _collision.native();}
	void input(float dt);
	void update(float dt);
//	render();

private:
	camera _cam;
	body_object _collision;
	glut_pool_window * _window;
	fps_look _look;
	fps_move _move;
};

void player::init(vec3 const & position, float fovy, float aspect_ratio, float near, float far, glut_pool_window * window)
{
	_cam = camera{fovy, aspect_ratio, near, far};

	float const mass = 90.0f;
	_collision = body_object{make_box_shape(btVector3{0.25, 0.25, 0.25}), mass, bullet_cast(position)};
	_collision.native()->setAngularFactor(0);  // nechcem aby sa hrac otacal pri kolizii
	_collision.native()->setActivationState(DISABLE_DEACTIVATION);

	_window = window;
	_look.init(_window, _collision.native());
	_move.init(&_window->in, _collision.native());
}

void player::input(float dt)
{
	_look.input(dt);
	_move.input(dt);
}

void player::link_with(rigid_body_world & world)
{
	world.link(_collision);
	_collision.native()->setGravity(btVector3{0,0,0});  // turn off object gravity
}

void player::update(float dt)
{
	_cam.position = glm_cast(_collision.position());
	_cam.rotation = glm_cast(_collision.rotation());
}

class scene_window : public ui::glut_pool_window
{
public:
	using base = ui::glut_pool_window;

	scene_window();
	void input(float dt) override;
	void update(float dt) override;
	void display() override;

private:
	enum class view_mode{free_view, player_view, locked_camera};

	mesh _plane;
	axis_object _axis;
	shader::program _prog;
	free_camera<scene_window> _cam;
	rigid_body_world _world;
	player _player;
	bool _enabled = false;
	view_mode _vmode = view_mode::free_view;
};

scene_window::scene_window()
	: _cam{radians(70.0f), aspect_ratio(), 0.01, 1000, *this}
{
	double const size = 10.0;
	_plane = make_plane_xz(vec3{0, 0, 0}, 2*size, 11, 11);
	_prog.from_file(program_path);
	_cam.get_camera().position = vec3{3,2,5};

	// physics
//	_world.world()->setGravity(btVector3{0,0,0});
	_player.init(vec3{0,1,0}, radians(70.0f), aspect_ratio(), 0.01, 1000, this);
	_player.link_with(_world);
}

void scene_window::display()
{
	camera * cam = &_cam.get_camera();
	if (_vmode == view_mode::player_view)
		cam = &_player.get_camera();

	_prog.use();
	_prog.uniform_variable("local_to_screen", cam->world_to_screen());
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);
	_plane.render();

	_axis.render(cam->view_projection());
	_world.debug_render(cam->view_projection());

	base::display();
}

void scene_window::input(float dt)
{
	if (in.key_up('1'))
		_vmode = view_mode::free_view;

	if (in.key_up('2'))
		_vmode = view_mode::player_view;

	if (in.key_up('3'))
		_vmode = view_mode::locked_camera;

	if (_vmode == view_mode::player_view || _vmode == view_mode::locked_camera)
		_player.input(dt);
	else
		_cam.input(dt);

	base::input(dt);
}

void scene_window::update(float dt)
{
	base::update(dt);
	_world.update(dt);
	_player.update(dt);
}

int main(int argc, char * argv[])
{
	scene_window w;
	w.start();
	return 0;
}

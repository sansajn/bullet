// pohybovanie statickym objektom * statyckym objektom sa neda hybat zmenou jeho rychlosti
#include "gl/window.hpp"
#include "gl/scene_object.hpp"
#include "gl/controllers.hpp"
#include "physics/physics.hpp"
#include "physics/physics_debug.hpp"

using glm::radians;

class scene_window : public ui::glut_pool_window
{
public:
	using base = ui::glut_pool_window;

	scene_window();
	void input(float dt) override;
	void update(float dt) override;
	void display() override;

private:
	axis_object _axis;
	gl::free_camera<scene_window> _cam;
	rigid_body_world _world;
	physics_object _cube;
	debug_drawer _ddraw;
};

scene_window::scene_window()
	: _cam{radians(70.0f), aspect_ratio(), 0.01, 1000, *this}
{
	_cube = physics_object{make_box_shape(btVector3{.5, .5, .5})};
	_world.world()->setGravity(btVector3{0,0,0});
	_world.add(_cube.body());
	_world.debug_drawer(&_ddraw);
	_ddraw.toggle_debug_flag(btIDebugDraw::DBG_MAX_DEBUG_DRAW_MODE);
	glClearColor(0,0,0,1);
}

void scene_window::display()
{
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);
	_axis.render(_cam.get_camera().view_projection());
	_world.debug_draw();
	base::display();
}

void scene_window::input(float dt)
{
	_cam.input(dt);
	base::input(dt);
}

void scene_window::update(float dt)
{
	base::update(dt);
	_world.update(dt);
	_ddraw.update(_cam.get_camera().view_projection());
}

int main(int argc, char * argv[])
{
	scene_window w;
	w.start();
	return 0;
}


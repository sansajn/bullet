// fyzika lekarnicky pouzitej prejdenim skrz nu (podobne ako v wolenstein 3d)
#include <vector>
#include "gl/window.hpp"
#include "gl/controllers.hpp"
#include "gl/scene_object.hpp"
#include "gl/label.hpp"
#include "physics/physics.hpp"
#include "geometry/utility.hpp"
#include <iostream>

using std::vector;
using glm::vec3;
using glm::mat3;
using glm::ivec2;
using glm::radians;
using gl::camera;
using geom::right;
using geom::forward;
using ui::glut_pool_window;

using namespace phys;

std::string const font_path = "/usr/share/fonts/truetype/freefont/FreeMono.ttf";

class fps_move  //!< wsad move in xz plane
{
public:
	fps_move() {}
	void init(ui::glut_pool_window::user_input * in, btRigidBody * body, float velocity = 2);
	void input(float dt);
	void controls(char forward, char backward, char left, char right);

private:
	enum class key {forward, backward, left, right, key_count};
	btRigidBody * _body;
	ui::glut_pool_window::user_input * _in;
	float _velocity;
	char _controls[(int)key::key_count+1];
};

class fps_look
{
public:
	fps_look() {}
	void init(ui::glut_pool_window * window, btRigidBody * body, float velocity = 1);
	void input(float dt);

private:
	ui::glut_pool_window * _window;
	btRigidBody * _body;
	float _velocity;
	bool _enabled = false;
};

class fps_player
{
public:
	fps_player() {}
	void init(btVector3 const & position, float fovy, float aspect_ratio, float near, float far, ui::glut_pool_window * window);
	gl::camera & get_camera() {return _cam;}
	void link_with(rigid_body_world & world);
	btRigidBody * body() const {return _collision.native();}
	btCollisionObject * collision() const {return _collision.native();}
	void input(float dt);
	void update(float dt);
//	render();

private:
	gl::camera _cam;
	body_object _collision;
	ui::glut_pool_window * _window;
	fps_look _look;
	fps_move _move;
};


class medkit_object
{
public:
	medkit_object() {}
	void init(btVector3 const & position);
	void link_with(rigid_body_world & world);
	btCollisionObject * collision() const {return _collision.native();}

private:
	trigger_object _collision;
};

void medkit_object::init(btVector3 const & position)
{
	_collision = trigger_object{make_box_shape(btVector3{.2, .1, .2}), position + btVector3{0, .15, 0}};  // must levitate over the ground to avoid collision
}

void medkit_object::link_with(rigid_body_world & world)
{
	world.link(_collision);
}


class medkit_world : public phys::rigid_body_world
{
public:
	using base = phys::rigid_body_world;
	enum class collision_object_type {player, medkit};

	using base::link;
	void link(fps_player & p);
	void link(medkit_object & m);
};

void medkit_world::link(fps_player & p)
{
	p.collision()->setUserIndex((int)collision_object_type::player);  // mark player
	p.link_with(*this);
}

void medkit_world::link(medkit_object & m)
{
	m.collision()->setUserIndex((int)collision_object_type::medkit);  // mark medkit
	m.link_with(*this);
}


class medkit_pick_listenner : public collision_listener
{
public:
	medkit_pick_listenner(vector<medkit_object *> & medkits, rigid_body_world & world) : _medkits{medkits}, _world{world} {}
	void collision_event(btCollisionObject * body0, btCollisionObject * body1) override;

private:
	vector<medkit_object *>::iterator find_medkit(btCollisionObject * medkit);

	unsigned count = 0;
	vector<medkit_object *> & _medkits;
	rigid_body_world & _world;
};

void medkit_pick_listenner::collision_event(btCollisionObject * body0, btCollisionObject * body1)
{
	btCollisionObject * medkit = nullptr;
	btCollisionObject * other = nullptr;
	if (((btCollisionObject *)body0)->getUserIndex() == (int)medkit_world::collision_object_type::medkit)
	{
		medkit = (btCollisionObject *)body0;
		other = (btCollisionObject *)body1;
	}
	else if (((btCollisionObject *)body1)->getUserIndex() == (int)medkit_world::collision_object_type::medkit)
	{
		medkit = (btCollisionObject *)body1;
		other = (btCollisionObject *)body0;
	}

	assert(medkit && "predpoklada kolizie iba medzi lekarnickou a niecim");

	if (medkit)
		std::cout << "medkit collision #" << ++count << std::endl;

	if (medkit && (other->getUserIndex() == (int)medkit_world::collision_object_type::player))
	{
		auto it = find_medkit(medkit);
		if (it != _medkits.end())
		{
			_world.native()->removeCollisionObject(medkit);  // vyber lekarnicku zo sveta
			delete *it;
			_medkits.erase(it);  // vymaz ju zo zoznamu
		}
	}
}

vector<medkit_object *>::iterator medkit_pick_listenner::find_medkit(btCollisionObject * medkit)
{
	for (auto it = _medkits.begin(); it != _medkits.end(); ++it)
		if ((*it)->collision() == medkit)
			return it;
	return _medkits.end();
}


class medkit_scene : public ui::glut_pool_window
{
public:
	using base = ui::glut_pool_window;

	medkit_scene();
	void input(float dt) override;
	void update(float dt) override;
	void display() override;

private:
	gl::free_camera<medkit_scene> _view;
	axis_object _axis;
	ui::label<medkit_scene> _info;

	// physics
	medkit_world _world;
	body_object _ground;

	fps_player _player;
	vector<medkit_object *> _medkits;
	medkit_pick_listenner _medkit_listenner;

	bool _camera_unlocked = false;
};

medkit_scene::medkit_scene()
	: base{parameters{}.name("medkit physics test")}
	, _view{radians(70.0f), aspect_ratio(), 0.01, 1000, *this}
	, _medkit_listenner{_medkits, _world}
{
	_view.get_camera().position = vec3{3, 3, 4};
	_view.get_camera().look_at(vec3{0,0,0});

	_info.init(0, 0, *this);
	_info.font(font_path, 16);

	_ground = body_object{make_box_shape(btVector3{10, .1, 10}), 0, btVector3{0, -.1, 0}};
	_world.link(_ground);

	// medkits
	for (int r = -1; r <= 1; ++r)
	{
		for (int c = -1; c <= 1; ++c)
		{
			_medkits.push_back(new medkit_object{});
			medkit_object *& m = _medkits.back();
			m->init(btVector3{c*1.0f, 0, r*1.0f});
			_world.link(*m);
		}
	}

	_world.add_collision_listener(&_medkit_listenner);

	_player.init(btVector3{-4,-.2,1}, radians(70.0f), aspect_ratio(), 0.01, 1000, this);
	_world.link(_player);

	glClearColor(0,0,0,1);
}

void medkit_scene::input(float dt)
{
	if (_camera_unlocked)
		_view.input(dt);
	else
		_player.input(dt);

	if (in.key_up('u'))
	{
		_camera_unlocked = true;
		_info.text("camera mode");
	}

	if (_camera_unlocked && in.key_up(27))  // esc
	{
		_camera_unlocked = false;
		_info.text("player mode");
	}

	base::input(dt);
}

void medkit_scene::update(float dt)
{
	base::update(dt);
	_world.update(dt);
}

void medkit_scene::display()
{
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);
	auto VP = _view.get_camera().view_projection();
	_axis.render(VP);
	_world.debug_render(VP);
	base::display();
}


int main(int argc, char * argv[])
{
	medkit_scene w;
	w.start();
	return 0;
}


void fps_move::init(glut_pool_window::user_input * in, btRigidBody * body, float velocity)
{
	assert(in && body && "invalid pointer");
	_in = in;
	_body = body;
	_velocity = velocity;
	strcpy(_controls, "wsad");  // default controls
}

void fps_move::input(float dt)
{
	vec3 vel{0,0,0};

	if (_in->one_of_key(_controls))
	{
		mat3 r = mat3_cast(glm_cast(_body->getOrientation()));
		vec3 right_dir = right(r);
		vec3 forward_dir = forward(r);

		if (_in->key(_controls[(int)key::forward]))
			vel -= forward_dir;
		if (_in->key(_controls[(int)key::backward]))
			vel += forward_dir;
		if (_in->key(_controls[(int)key::left]))
			vel -= right_dir;
		if (_in->key(_controls[(int)key::right]))
			vel += right_dir;

		vel.y = 0;  // chcem sa pohybovat iba po rovine
		vel *= _velocity;
	}

	_body->setLinearVelocity(bullet_cast(vel));
}

void fps_look::init(glut_pool_window * window, btRigidBody * body, float velocity)
{
	_window = window;
	_body = body;
	_velocity = velocity;
}

void fps_look::input(float dt)
{
	ivec2 screen_center = _window->center();

	if (_window->in.mouse(glut_pool_window::button::left) && !_enabled)
	{
		_enabled = true;
		glutSetCursor(GLUT_CURSOR_NONE);
		glutWarpPointer(screen_center.x, screen_center.y);
		return;
	}

	if (_window->in.key_up(27) && _enabled)  // esc
	{
		_enabled = false;
		glutSetCursor(GLUT_CURSOR_INHERIT);
	}

	if (!_enabled)
		return;

	float const angular_movement = 0.1f;
	ivec2 delta = _window->in.mouse_position() - screen_center;

	if (delta.x != 0)
	{
		float angle = radians(angular_movement * delta.x);
		btTransform transf = _body->getCenterOfMassTransform();
		btQuaternion r = btQuaternion{btVector3{0,1,0}, -angle} * transf.getRotation();
		_body->setCenterOfMassTransform(btTransform{r, transf.getOrigin()});
	}

	if (delta.y != 0)
	{
		float angle = radians(angular_movement * delta.y);
		btTransform transf = _body->getCenterOfMassTransform();
		btVector3 right_dir = transf.getBasis().getColumn(0);
		btQuaternion r = btQuaternion{right_dir, -angle} * transf.getRotation();
		_body->setCenterOfMassTransform(btTransform{r, transf.getOrigin()});
	}

	if (delta.x != 0 || delta.y != 0)
		glutWarpPointer(screen_center.x, screen_center.y);
}

void fps_player::init(btVector3 const & position, float fovy, float aspect_ratio, float near, float far, ui::glut_pool_window * window)
{
	_cam = camera{fovy, aspect_ratio, near, far};

	float const mass = 90.0f;
	_collision = body_object{make_box_shape(btVector3{0.25, 0.25, 0.25}), mass, position + btVector3{0, 0.5, 0}};
	_collision.native()->setAngularFactor(0);  // nechcem aby sa hrac otacal pri kolizii
	_collision.native()->setActivationState(DISABLE_DEACTIVATION);

	_window = window;
	_look.init(_window, _collision.native());
	_move.init(&_window->in, _collision.native());
}

void fps_player::input(float dt)
{
	_look.input(dt);
	_move.input(dt);
}

void fps_player::link_with(rigid_body_world & world)
{
	world.link(_collision);
	_collision.native()->setGravity(btVector3{0,0,0});  // turn off object gravity
}

void fps_player::update(float dt)
{
	_cam.position = glm_cast(_collision.position());
	_cam.rotation = glm_cast(_collision.rotation());
}

// kolizie a ovladanie hracskej postavy
#include <vector>
#include <glm/gtx/transform.hpp>
#include <bullet/btBulletCollisionCommon.h>
#include <bullet/BulletCollision/CollisionShapes/btBox2dShape.h>
#include "mesh.hpp"
#include "camera.hpp"
#include "controllers.hpp"
#include "program.hpp"
#include "window.hpp"
#include "scene_object.hpp"
#include "physics/physics.hpp"
#include "physics/debug_draw.hpp"
#include "label.hpp"
#include "texture.hpp"

using std::string;
using std::to_string;
using std::vector;
using std::shared_ptr;
using glm::vec2;
using glm::vec3;
using glm::mat3;
using glm::mat4;
using glm::quat;
using glm::mat4_cast;
using glm::radians;
using glm::normalize;
using glm::scale;
using glm::translate;
using gl::mesh;
using gl::vertex;
using gl::make_plane_xz;
using gl::mesh_from_vertices;
using gl::make_cube;
using gl::make_quad_xz;
using gl::camera;
using gl::free_look;
using gl::free_move;
using ui::window;
using ui::glut_pool_window;
using ui::label;

using namespace phys;

string shaded_shader_path = "shaders/shaded.glsl";
string texture_shader_path = "shaders/texture.glsl";
string font_path = "/usr/share/fonts/truetype/oxygen/OxygenMono-Regular.ttf";
string finish_texture_path = "assets/textures/maze_finish.png";

/* kde sa vyrenderuje stena je urcene cislom 1 az 4, takto

				 -
  1|, 2, |3, 4
		-

pozicia hraca je oznacena ako 5 a ciel ako 6. */
uint8_t level_data[] = {
	6, 2, 2, 1, 2, 0,
	0, 0, 1, 0, 2, 2,
	2, 1, 0, 0, 0, 0,
	0, 1, 2, 2, 0, 3,
	0, 1, 0, 0, 1, 0,
	0, 0, 0, 0, 4, 5
};

struct player
{
public:
	player() {}
	player(vec3 position, rigid_body_world & world);
	void render(camera & cam, shader::program & prog, bool in_finish);
	void input(float dt, glut_pool_window::user_input & in);

private:
	body_object _phys;
	mesh _mesh;
};

player::player(vec3 position, rigid_body_world & world)
{
	_mesh = make_cube();
	_phys = body_object{shared_ptr<btCollisionShape>{
		new btBoxShape{0.5*btVector3{0.5, 0.5, 0.5}}}, 10, bullet_cast(position)};
	_phys.native()->setActivationState(DISABLE_DEACTIVATION);
	world.link(_phys);
}

void player::render(camera & cam, shader::program & prog, bool in_finish)
{
	mat4 M =
		translate(glm_cast(_phys.position())) *
		mat4_cast(glm_cast(_phys.rotation())) *
		scale(vec3{0.5});

	prog.uniform_variable("local_to_screen", cam.view_projection() * M);

	auto color = prog.uniform_variable("color");
	if (in_finish)
		*color = rgb::red;
	else
		*color = rgb::green;

	_mesh.render();
}

void player::input(float dt, glut_pool_window::user_input & in)
{
	if (!in.one_of_key("wsad"))
		return;

	btVector3 velocity = _phys.native()->getLinearVelocity();

	if (in.key('w'))
		velocity[2] = -1;

	if (in.key('s'))
		velocity[2] = 1;

	if (in.key('a'))
		velocity[0] = -1;

	if (in.key('d'))
		velocity[0] = 1;

	_phys.native()->setLinearVelocity(velocity);
}

struct maze_collisions : public collision_listener
{
	maze_collisions(vector<body_object> * walls) : walls{walls} {}
	void collision_event(btCollisionObject * body0, btCollisionObject * body1) override;
	unsigned count = 0;
	bool in_finish = false;
	vector<body_object> * walls;
	btCollisionObject * finish = nullptr;
};

void maze_collisions::collision_event(btCollisionObject * body0, btCollisionObject * body1)
{
	assert(finish && "finish trigger reference not set");

	if (body0 == finish || body1 == finish)
	{
		in_finish = true;
		return;
	}

	for (auto & o : *walls)  // TODO: toto je pomale, nemoze body obsahovat nejaky flag ?
		if (o.native() == body0 || o.native() == body1)  // hits wall
			++count;
}

class scene_window : public glut_pool_window
{
public:
	using base = glut_pool_window;

	scene_window();
	~scene_window() {}
	void input(float dt) override;
	void update(float dt) override;
	void display() override;

private:
	void generate_level();
	void generate_physics();
	
	// game
	float _total_time = 0;
	mesh _plane, _walls;
	player _player;
	vec3 _initial_player_pos;
	vec3 _finish_pos;

	// physics
	rigid_body_world _world;
	body_object _phys_plane;
	vector<body_object> _phys_objs;
	trigger_object _finish_trigger;
	debug_draw_impl _ddraw;
	maze_collisions _collisions;

	// assets
	texture2d _finish;
	mesh _finish_quad;

	// render
	axis_object _axis;
	light_object _light;
	camera _cam;
	free_look<scene_window> _look;
	free_move<scene_window> _move;
	shader::program _shaded, _textured;
	label<scene_window> _lbl_hits;
};

void scene_window::generate_level()
{
	// plane
	int plane_size = ceil(sqrt(sizeof(level_data)));
	_plane = make_plane_xz(plane_size+1, plane_size+1, plane_size);

	// walls
	vector<vertex> verts;
	vector<unsigned> indices;
	for (int y = 0; y < plane_size; ++y)
	{
		for (int x = 0; x < plane_size; ++x)
		{
			uint8_t val = level_data[y*plane_size + x];
			if (val == 1)
			{
				unsigned size = verts.size();
				verts.emplace_back(vec3{x+1, 0, y+1 - plane_size}, vec3{1,0,0});
				verts.emplace_back(vec3{x+1, 0, y - plane_size}, vec3{1,0,0});
				verts.emplace_back(vec3{x+1, 1, y - plane_size}, vec3{1,0,0});
				verts.emplace_back(vec3{x+1, 1, y+1 - plane_size}, vec3{1,0,0});
				indices.insert(indices.end(), {size, size+1, size+2, size+2, size+3, size});
			}
			else if (val == 2)
			{
				unsigned size = verts.size();
				verts.emplace_back(vec3{x, 0, y+1 - plane_size}, vec3{0,0,1});
				verts.emplace_back(vec3{x+1, 0, y+1 - plane_size}, vec3{0,0,1});
				verts.emplace_back(vec3{x+1, 1, y+1 - plane_size}, vec3{0,0,1});
				verts.emplace_back(vec3{x, 1, y+1 - plane_size}, vec3{0,0,1});
				indices.insert(indices.end(), {size, size+1, size+2, size+2, size+3, size});
			}
			else if (val == 3)
			{
				unsigned size = verts.size();
				verts.emplace_back(vec3{x, 0, y - plane_size}, vec3{-1,0,0});
				verts.emplace_back(vec3{x, 0, y+1 - plane_size}, vec3{-1,0,0});
				verts.emplace_back(vec3{x, 1, y+1 - plane_size}, vec3{-1,0,0});
				verts.emplace_back(vec3{x, 1, y - plane_size}, vec3{-1,0,0});
				indices.insert(indices.end(), {size, size+1, size+2, size+2, size+3, size});
			}
			else if (val == 4)
			{
				unsigned size = verts.size();
				verts.emplace_back(vec3{x, 0, y - plane_size}, vec3{0,0,-1});
				verts.emplace_back(vec3{x+1, 0, y - plane_size}, vec3{0,0,-1});
				verts.emplace_back(vec3{x+1, 1, y - plane_size}, vec3{0,0,-1});
				verts.emplace_back(vec3{x, 1, y - plane_size}, vec3{0,0,-1});
				indices.insert(indices.end(), {size, size+1, size+2, size+2, size+3, size});
			}
			else if (val == 5)  // player
				_initial_player_pos = vec3{x+0.5, 0.5, y - 0.5 - plane_size};
			else if (val == 6)  // finish
				_finish_pos = vec3{x+0.5, 0, y+0.5 - plane_size};
		}
	}  // for y

	_walls = mesh_from_vertices(verts, indices);
}

void scene_window::generate_physics()
{
	// plane
	int plane_size = ceil(sqrt(sizeof(level_data)));

	shared_ptr<btCollisionShape> plane_shape{new btBoxShape{btVector3(plane_size/2.0, 0.1, plane_size/2.0)}};
	_phys_plane = body_object{plane_shape, 0, btVector3(plane_size/2.0, -0.1, -plane_size/2.0)};
	_world.link(_phys_plane);

	// walls
	shared_ptr<btCollisionShape> wall_shape{new btBox2dShape{btVector3{0.5, 0.5, 0}}};

	for (int y = 0; y < plane_size; ++y)
	{
		for (int x = 0; x < plane_size; ++x)
		{
			uint8_t val = level_data[y*plane_size + x];
			if (val == 1)
			{
				_phys_objs.emplace_back(wall_shape, 0, btVector3(x+1, 0.5, y+0.5 - plane_size),
					btQuaternion{btVector3{0,1,0}, radians(90.0f)});
				_world.link(_phys_objs.back());
			}
			else if (val == 2)
			{
				_phys_objs.emplace_back(wall_shape, 0, btVector3(x+0.5, 0.5, y+1 - plane_size));
				_world.link(_phys_objs.back());
			}
			else if (val == 3)
			{
				_phys_objs.emplace_back(wall_shape, 0, btVector3(x, 0.5, y+0.5 - plane_size),
					btQuaternion{btVector3{0,1,0}, radians(270.0f)});
				_world.link(_phys_objs.back());
			}
			else if (val == 4)
			{
				_phys_objs.emplace_back(wall_shape, 0, btVector3(x+0.5, 0.5, y - plane_size),
					btQuaternion{btVector3{0,1,0}, radians(180.0f)});
				_world.link(_phys_objs.back());
			}
		}
	}  // y

	// player
	_player = player{_initial_player_pos, _world};

	// finish trigger
	_finish_trigger = trigger_object{make_box_shape(btVector3{0.2, 0.1, 0.2}), btVector3{bullet_cast(_finish_pos + vec3{0, 0.2, 0})}};
	_world.link(_finish_trigger);
	_collisions.finish = _finish_trigger.native();
}

scene_window::scene_window()
	: base{parameters{}.name("Maze sample")}, _collisions{&_phys_objs}, _look{_cam, *this}, _move{_cam, *this}
{
	generate_level();
	generate_physics();
	_world.add_collision_listener(&_collisions);
	_world.debug_drawer(&_ddraw);
	_finish = texture2d{finish_texture_path};
	_finish_quad = make_quad_xz(vec2{-0.5, -0.5}, 1);

	_cam = camera{vec3{6, 4, 0}, radians(70.0f), aspect_ratio(), 0.01, 1000};
	_cam.look_at(vec3{3, 0, -3});
	_shaded.from_file(shaded_shader_path);
	_textured.from_file(texture_shader_path);

	_lbl_hits.init(0, 0, *this);
	_lbl_hits.font(font_path, 18);
	_lbl_hits.text("hits: 0");

	glClearColor(0, 0, 0, 1);
}

void scene_window::display()
{
	vec3 const light_pos{1,3,2};

	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	mat4 world_to_screen = _cam.view_projection();

	_shaded.use();
	_shaded.uniform_variable("local_to_screen", world_to_screen);
	_shaded.uniform_variable("normal_to_world", mat3{1});
	_shaded.uniform_variable("light_dir", normalize(light_pos));
	_shaded.uniform_variable("color", rgb::gray);

	glEnable(GL_DEPTH_TEST);
	_plane.render();
	// finish
	mat4 M = translate(_finish_pos) * scale(vec3{0.8});
	_textured.use();
	_textured.uniform_variable("local_to_screen", world_to_screen * M);
	_finish.bind(0);
	_textured.uniform_variable("diff_tex", 0);
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	_finish_quad.render();
	glDisable(GL_BLEND);
	glEnable(GL_DEPTH_TEST);

	_shaded.use();
	_walls.render();
	_player.render(_cam, _shaded, _collisions.in_finish);

	// hits label
	if (_total_time > 1.0)
	{
		string text;
		if (_collisions.in_finish)
			text = "you are done with " + to_string(_collisions.count) + " hits";
		else
			text = "hits: " + to_string(_collisions.count);
		_lbl_hits.text(text);
	}
	_lbl_hits.render();

	// debug
	_axis.render(_cam.view_projection());
	_light.render(_cam.view_projection() * translate(light_pos));
	_world.debug_render(_cam.view_projection());

	base::display();
}

void scene_window::update(float dt)
{
	base::update(dt);
	_total_time += dt;
	_world.update(dt);
	_ddraw.update(_cam.view_projection());
}

void scene_window::input(float dt)
{
	_look.input(dt);
//	_move.input(dt);

	if (!_collisions.in_finish)
		_player.input(dt, in);

	if (in.key_up('g'))
		_ddraw.toggle_debug_flag(btIDebugDraw::DBG_MAX_DEBUG_DRAW_MODE);

	base::input(dt);
}

int main(int argc, char * argv[])
{
	scene_window w;
	w.start();
	return 0;
}
